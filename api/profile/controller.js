const httpStatus = require('http-status')
const Profile = require("./model")

exports.create = async (req, res) => {
    let profile = await Profile.create({ description: req.body.description });
    res.send(profile);
}

exports.findAll = async (req, res) => {
    try {
        let profiles = await Profile.findAll();
        res.send(profiles);
    } catch (err) {
        console.log("Erro ao buscar perfis: " + err);
        res.status(httpStatus.INTERNAL_SERVER_ERROR).end("Erro ao buscar perfis.");
    }

}
